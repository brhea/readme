---
layout: markdown_page
title: "Brian Rhea's README"
description: "Brian is a Product Manager at GitLab in Product Operations."
---

## Brian's README

Hi, I'm Brian Rhea (sounds like Ray). I grew up in Paris, Texas, a small town that very much resembles [Friday Night Lights](https://en.wikipedia.org/wiki/Friday_Night_Lights_(TV_series)). A formative part of my life was watching my dad grow a mail-order computer repair business from a desk in his bedroom, to a table in the kitchen, to the whole garage, to multiple offices in town. This part of my story deeply informs the importance I place on [autonomy, mastery, and purpose](https://www.youtube.com/watch?v=rrkrvAUbU9Y).

My degree is in Art Education and I spent the first five years after college teaching Middle School Art in Carrollton, Texas. As much as I loved the classroom, the structure of public education wasn't a good fit for me and I found myself gravitating back toward tech.

In 2012, I joined a startup in Boulder, which allowed my wife and I to move to Colorado. This was a big moment in our lives and we celebrate the story behind it as "[Something has happened day](https://brianrhea.com/newsletter/something-has-happened/)."

I love the outdoors: fishing, hiking, camping, and backpacking. I'm a big boardgame nerd and I've even started trying my hand at designing a few games of my own. My wife, our three kids, and our dog, River, are always on the lookout for a new adventure!

* [@brhea at GitLab](https://gitlab.com/brhea)
* [My Team Page Entry](/company/team/#brhea)

## Communicating and working with me

* I check my email a few times per day and do my best to respond within 24-48 hours.
* I close Slack throughout the day in order to reduce distractions, so don't be surprised if I don't reply to a DM even if we just got off a call.
* I have burned myself out a couple of times in my career through long hours, always-on mentality, and chronic urgency. If I come across as dispassionate, it's not that I don't care, it's that I'm trying to take a slow & steady approach to my work.
* In group conversations, I tend to speak up much less than my fair share. Believe me, I'm listening and I have ideas. But I tend to listen to multiple perspectives, synthesize those view points and find commonalities, strengths, and weaknesses before deciding where I stand.

## Values

* The GitLab values and substantiating values that resonate deeply with me are:
  * Results
    * [Measure results, not hours](https://about.gitlab.com/handbook/values/#measure-results-not-hours)
    * [Give agency](https://about.gitlab.com/handbook/values/#give-agency)
    * [Accepting uncertainty](https://about.gitlab.com/handbook/values/#accepting-uncertainty)
  * Efficiency
    * [Boring solutions](https://about.gitlab.com/handbook/values/#boring-solutions)
    * [Be respectful of others' time](https://about.gitlab.com/handbook/values/#be-respectful-of-others-time)
    * [Embrace change](https://about.gitlab.com/handbook/values/#be-respectful-of-others-time)
  * Iteration
    * [MVC](https://about.gitlab.com/handbook/values/#minimal-viable-change-mvc)
    * [Low level of shame](https://about.gitlab.com/handbook/values/#low-level-of-shame)

## Principles

There are a handful of ideas, principles, eponymous laws and what-have-you that I try to defer to when making decisions.

* Everyone is doing the best they can with what they have.
* Wisdom is having both an insight and the virtue to live it out.

* Gall's Law: A complex system that works evolved from a simple system that worked. The inverse is also true: A complex system designed from scratch never works and cannot be made to work. *So, start with a simple thing that works.*
* Occam's Razor: The simplest explanation is usually the best one. *So, don't shove round pegs in square holes to affirm bias.*
* Parkinson’s Law: Work expands so as to fill the time available for its completion. *So, keep things simple and if you get done early, don't feel guilty. Walk away.*

* Ownership bias: People are more likely to over-value an object they own than acquire that same object when they do not own it. *So, discount certainty in your own belief; extend that certainty to the opposition.*

## Personality tests

* Enneagram: [Type 9 The Peacemaker](https://www.enneagraminstitute.com/type-9)
* DiSC: [Result-Oriented Pattern](https://www.discprofiles4u.com/blog/2012/disc-classic-profile-results-oriented-pattern-3-of-16-2/)
* Myers-Briggs: [INTP](https://www.16personalities.com/intp-personality)
