---
layout: markdown_page
title: "Brian's Weekly Snippets"
---

# Quarterly Priorities:
A rough view of how I intend to spend my time across quarters.

## FY22-Q4
| Theme | Notes | Percent |
| ------ | ------ | ------ |
| Onboarding | PM and product onboarding, 100 day plan | 20% |
| Release Post | Shadow 14.6 and 14.7 | 35% |
| ProdOps Backlog | Fine-tune development environment and contribute to the ProdOps backlog | 30% |
| Ally | Gain a better than average understanding of Ally.io and our OKR process | 15% |
| Building relationships with team | Coffee chats, company syncs, team meetings | 5% |
| Personal Development | Reading, Learning, Training | 5% |

# [Snippets](http://blog.idonethis.com/google-snippets-internal-tool/)



## 2022-01-10
- Removals
- Triage-ops tests and education
- Release post support
- Coffee chats
- OKRs
- ProdOps Direction
- Breaking Changes workflow

## 2022-01-03
- Removals in Docs
- Monitor Release Post
- Triage-Ops Education and Exploration
- Setup GDK 
## 2021-12-27
- Sick and Holidays
## 2021-12-20
- Update the ProdOps board
- Breaking Changes and Removals Process
- Monitor Ally threads and Issues
- Triage-Ops Education and Exploration
- ## 2021-12-13
- Qualtrics meeting
- Breaking changes epic
- Follow ups from Ally training
  - Organize resources
  - Further education and familiarity
  - Adding ProdOps OKRs to Ally
- ProdOps board workflow
- Finalizing ProdOps issue automations
## 2021-12-06
- Deprecations & Removals
- Additional support on Issue and MR Templates
- ProdOps Office Hours
- Dev Environment fine-tuning (VS Code)
- Workflow fine-tuning (Gmail filters, Chrome bookmarks)
- Ally Training

## 2021-11-29
- Onboarding (week 4)
- Attend pre-training sync with Ally
- MR template for Product Handbook changes
- Automations for quarterly ProdOps Issues
- Product OKRs Structure and Editing
- Brush up on Deprecations & Removals in Docs & Release Posts

## 2021-11-22
- Onboarding (week 3)
- Ally education
- Setup Dev environment
- Tinker just a bit with [Issue automations](https://gitlab.com/gitlab-com/Product/-/merge_requests/331)

## 2021-11-15
- PTO

## 2021-11-08
- Onboarding (week 2)
- Attend ProdOps Office Hours
- Attend Ally.io training calls
- Coffee chats

## 2021-11-01
- Onboarding (week 1)
